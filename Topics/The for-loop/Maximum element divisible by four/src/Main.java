import java.util.Scanner;

class Main {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        int n = scanner.nextInt();
        int max = 0;

        for (int i = 1; i <= n; i++) {
            int m = scanner.nextInt();
            if (m % 4 == 0 && max < m) {
                max = m;
            }
        }

        System.out.println(max);
    }
}