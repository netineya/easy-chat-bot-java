import java.util.Scanner;

class Main {
    public static void main(String[] args) {

        Scanner scanner = new Scanner(System.in);

        int i = scanner.nextInt();
        int n = scanner.nextInt();
        int sum = 0;

        for (; i <= n; i++) {
            sum += i;
        }

        System.out.println(sum);
    }
}