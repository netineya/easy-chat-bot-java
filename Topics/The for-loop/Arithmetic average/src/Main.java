import java.util.Scanner;

class Main {
    public static void main(String[] args) {

        Scanner scanner = new Scanner(System.in);

        int n1 = scanner.nextInt();
        int n2 = scanner.nextInt();
        int count = 0;
        double result = 0;

        for (; n1 <= n2; n1++) {
            if (n1 % 3 == 0) {
                count++;
                result += n1;
            }
        }
        System.out.println(result / count);
    }
}