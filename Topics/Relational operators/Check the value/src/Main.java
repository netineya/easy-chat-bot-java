import java.util.Scanner;

class Main {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        int point = scanner.nextInt();

        System.out.println(point < 10 && point > 0);
    }
}