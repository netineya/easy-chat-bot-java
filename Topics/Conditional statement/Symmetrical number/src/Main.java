import java.util.Scanner;

class Main {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        int n = scanner.nextInt();
        int i;
        int j;
        int j2;

        i = n / 100;
        j = n % 100;

        j = j % 10 * 10 + j / 10;
        j2 = j % 100;
        if (i == j2) {
            System.out.println(1);
        } else {
            System.out.println(37);
        }
    }
}